import pygraphviz as pgv
import networkx as nx
import matplotlib.pyplot as plt
import sys
import os
import shutil
from networkx.algorithms.traversal.depth_first_search import dfs_tree

def read_dot(path):
	return nx.DiGraph(pgv.AGraph(path))

def write_dot(G, path):
	agraph = nx.to_agraph(G)
	agraph.write(path)

def get_nodes(G, attr, val):
	res = []
	for n in G.node:
		if attr in G.node[n] and G.node[n][attr] == val:
			res.append(n)
	if (res == []):
		raise Exception('Node with attribute ' + attr + ' == ' + str(val) + ' not found')
	return res

def save_graph(G, aog1_path, aog2_path, directory):
	if not os.path.exists(directory):
		os.makedirs(directory)
		os.makedirs(directory + '/src/mind_graph/src/icons')
	for n in G.node:
		# copy data
		graph_source, old_id = G.node[n]['old_id'][0:2], G.node[n]['old_id'][3:]
		if (graph_source == 'g1'):
			graph_path = aog1_path
		elif (graph_source == 'g2'):
			graph_path = aog2_path + '/' + old_id
		node_path = graph_path + '/' + old_id
		if (os.path.exists(node_path) and os.path.isdir(node_path)):
			shutil.copytree(node_path, directory + '/' + str(n))
		# fix image path
		if 'image' in G.node[n]:
			icon_path = './src/mind_graph/src/icons/' + str(n) + '.png'
			shutil.copy(G.node[n]['image'], directory + '/' + icon_path)
			G.node[n]['image'] = icon_path
	write_dot(merged_tree, directory + '/' + 'aog.dot')

def merge_nodes(G, node1, node2):
	"""
	Merge node2 into node1 in G
	"""
	for n1,n2,data in G.edges(data=True):
		if (n1 == node2):
			G.add_edge(node1, n2)
		elif (n2 == node2):
			G.add_edge(n1, node1)
	G.remove_node(node2)

if __name__ == "__main__":

	if (len(sys.argv) < 2):
		print 'Usage: ', sys.argv[0], 'merge_spec.txt'
		sys.exit()

	fin = open(sys.argv[1])
	aog1_path = fin.readline()[:-1]
	aog2_path = fin.readline()[:-1]
	matches = []
	for line in fin:
		matches.append(tuple(map(lambda x: str(int(x)),  line.split(','))))

	# Load data
	G1 = read_dot(aog1_path + '/aog.dot')
	G2 = read_dot(aog2_path + '/aog.dot')

	# Store node id in a different attribute for merge purpose
	s1 = set(map(lambda x: x[0], matches))
	s2 = set(map(lambda x: x[1], matches))
	for n in G1.node:
		G1.node[n]['old_id'] = 'g1_' + n
	for n in G2.node:
		G2.node[n]['old_id'] = 'g2_' + n

	# Use absolute path for icons for better visualization
	for n in G1.node:
		if 'image' in G1.node[n]:
			G1.node[n]['image'] = aog1_path + '/' + G1.node[n]['image'] 
	for n in G2.node:
		if 'image' in G2.node[n]:
			G2.node[n]['image'] = aog2_path + '/' + G2.node[n]['image'] 

	# Take disjoint union of state transition tree
	tshirt_tree_1 = G1.subgraph(dfs_tree(G1, get_nodes(G1, 'name', 'cloth')[0]))
	tshirt_tree_2 = G2.subgraph(dfs_tree(G2, get_nodes(G2, 'name', 'cloth')[0]))
	merged_tree = nx.disjoint_union(tshirt_tree_1, tshirt_tree_2)

	# Merge nodes that matches
	for id1, id2 in matches:
		node1 = get_nodes(merged_tree, 'old_id', 'g1_' + id1)[0]
		node2 = get_nodes(merged_tree, 'old_id', 'g2_' + id2)[0]
		merge_nodes(merged_tree, node1, node2)

	# Save
	save_graph(merged_tree, aog1_path, aog2_path,'./aog_merged')
